class: title

# Talk Failure to me
## Eduard Thamm

???

Welcome and introduction

---

# Content

- What is this Failure Mode thingy
- What does designing for failure mean
- Why should I care
- Questions and Discussion

???

We will talk today about

- what failure modes are
- what designing for failure means
- why you should care about this

---

# What is a Failure Mode

For our purposes:

## "A failure mode is a cause of failure or one possible way a system can fail."

There are other definitions but let us work from here.

???

- Failure mode and effects analysis (FMEA) aims at identifying potential failure modes, causes and effects in systems
- 1950 initially military
- inductive reasoning based
- one of the first structured and systematic techniques
- Component vs System

---

# Which Fail Actions exist

- Fail Open
- Fail Closed
- Fail Last
- Fail Safe
- Fail Over

???
Remember some of these terms come from valves.

- On failure the system remains "functional". Think car breaks.
- On failure the system gets "non-functional". Think truck breaks.
- On failure the component retains the last state.
- On failure the component tries isolating the system from its failure. Think bypass switch.
- On failure the system is isolated through component function isolation. Think redundant components.

---

# Resiliency

- Fragile
- Robust
- Resilient
- (Anti-Fragile)

???

- Does not tolerate any failure. Will break if put under stress.
- Can handle some stress or failure but will eventually break. Does not adapt.
- Can adapt to stress and failure. Can still break but will do so way later than robust. Will also not degrade or degrade later.
- Systems that become stronger when put under stress. Autonomous repair and such.

---

# Designing for Failure

- Redundancy
- The concept of human error
- Back off
- Circuit breaking
- Bulkheads
- Safeguard against success

???

- Remember A = 1-(1-Ax)**N so 2 99% systems in parallel have 4-nines
- If you have built a system in which single component failure can lead to disaster it is a bad system
- Be a good citizen if your neighbor is in trouble give them some space.
- Isolate components from each other to prevent cascades.
- Build compartments in your system and keep failures confined to those.
- Lots of companies have been brought down by being unable to handle the load
- Isolation/Modularity/Redundancy

---

# Why should I care

.left-column[
  ![Tschernobyl](assets/tschernobyl.jpg)
  ![Fukushima](assets/fukushima.jpg)
]

.right-column[
  ![Baumgarten1](assets/baumgarten_1.jpeg)
  ![Baumgarten2](assets/baumgarten_2.jpg)
]

???

- Because you do not know where your code might end up
- Because you chose to be an engineer
- FMEA is a core task of reliability/safety/quality engineering

---

class: title

# Questions

---

class: title

# Thank you
